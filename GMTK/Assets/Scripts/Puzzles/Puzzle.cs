﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Puzzle : MonoBehaviour
{
    public float puzzleTime = 30f;
    public int targetScore = 10;
    public Animator targetAnim;
    public Dialogue entryDialogue;
    public Dialogue successDialogue;
    public Dialogue failDialogue;

    protected bool active = false;
    protected Timer puzzleTimer = new Timer();    
    
    //Score
    protected int currentScore = 0;

    public virtual void Awake()
    {
        puzzleTimer.Initialize(puzzleTime, false, false);
    }

    public int CurrentScore()
    {
        return currentScore;
    }

    public void OverrideTimer(float time)
    {
        puzzleTimer = new Timer();
        puzzleTimer.Initialize(time, false, false);
    }

    public virtual void Update()
    {
        if(active == true)
        {
            if(puzzleTimer.QueryTimer() == true)
            {
                if(puzzleTimer.UpdateTimer() == true)
                {
                    Puzzle p = GameManager.GetCurrentPuzzle();
                    if(Complete() == true && WinConditions() == true)
                    {
                        DialogueManager.instance.ActivateDialogue(successDialogue);
                        if (p.GetType() != typeof(BossFight))
                        {
                            GameManager.Progress(targetAnim, successDialogue.CurrentMessage().clip.length);
                        }
                        else
                        {
                            GameManager.Progress(targetAnim, successDialogue.CurrentMessage().clip.length, true);
                        }
                    }
                    else
                    {
                        DialogueManager.instance.ActivateDialogue(failDialogue);
                        GameManager.LoadCheckpoint(DialogueManager.currentDialogue.CurrentMessage().clip.length);
                    }
                }
            }
        }
    }

    public void ActivatePuzzleTimer()
    {
        puzzleTimer.StartTimer();
    }

    public virtual bool Complete()
    {
        HUD.instance.SetTargetText("");
        if (active == true && GameManager.GetCurrentPuzzle() == this)
        {
            HUD.instance.SetScoreText(0, 0);
            active = false;
            GameManager.SetPuzzle(null);
            DeactivatePuzzle();
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AddScore(int value)
    {
        if (value != 0)
        {
            currentScore += value;
            if (currentScore < 0)
            {
                currentScore = 0;
            }
        }
        else
        {
            currentScore = 0;
        }
    }

    private bool WinConditions()
    {
        if (currentScore >= targetScore)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public abstract bool ActivatePuzzle();
    public abstract void DeactivatePuzzle();
}
