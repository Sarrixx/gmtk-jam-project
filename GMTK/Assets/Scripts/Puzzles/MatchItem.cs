﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchItem : MonoBehaviour
{
    public TriggerTag triggerType = TriggerTag.none;

    private MatchItemTrigger target;

    public void SetTarget(MatchItemTrigger t)
    {
        if(target == null)
        {
            target = t;
        }
    }

    public MatchItemTrigger Target()
    {
        return target;
    }
}
