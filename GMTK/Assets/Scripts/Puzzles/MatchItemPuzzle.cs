﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchItemPuzzle : Puzzle
{
    public List<MatchItemTrigger> triggers;
    public MatchItem[] items;
    public int poolLimit = 30;
    public int activeLimit = 10;
    public float spawnTime = 5f;
    public float spawnForce = 5f;
    public Transform spawnPoint;

    //Object pool
    private List<MatchItem> inactivePool = new List<MatchItem>();
    private List<MatchItem> activePool = new List<MatchItem>();
    private Transform poolRoot;
    private Transform inactiveRoot;
    private Transform activeRoot;
    //Spawn timer
    private Timer spawnTimer = new Timer();

    public override void Awake()
    {
        base.Awake();
        //spawn object pool
        poolRoot = Instantiate(new GameObject("Match Item Pool"), transform).transform;
        inactiveRoot = Instantiate(new GameObject("Inactive Pool"), poolRoot).transform;
        activeRoot = Instantiate(new GameObject("Active Pool"), poolRoot).transform;
        foreach (MatchItemTrigger trigger in triggers)
        {
            trigger.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        if (items.Length > 0 && poolLimit > 0)
        {
            for (int i = 0; i < poolLimit; i++)
            {
                MatchItem spawn = Instantiate(items[Random.Range(0, items.Length)], spawnPoint.position, Quaternion.identity, inactiveRoot);
                if(spawn.triggerType != TriggerTag.none)
                {
                    foreach(MatchItemTrigger trigger in triggers)
                    {
                        if(trigger.triggerTag == spawn.triggerType)
                        {
                            spawn.SetTarget(trigger);
                        }
                    }
                }
                AddInactiveItem(spawn);
            }

        }
    }

    public override void Update()
    {
        base.Update();
        if (active == true)
        {
            if (spawnTimer.QueryTimer() == true)
            {
                if (spawnTimer.UpdateTimer() == true && activePool.Count < activeLimit)
                {
                    MatchItem item = inactivePool[Random.Range(0, inactivePool.Count)];
                    item.SetTarget(triggers[Random.Range(0, triggers.Count)]);
                    AddActiveItem(item);
                    spawnTimer.ResetTimer(true);
                }
            }
            else if (QueryActivePoolRoom() == true)
            {
                spawnTimer.ResetTimer(true);
            }
        }
    }

    public override bool ActivatePuzzle()
    {
        if (active == false)
        {
            currentScore = 0;
            HUD.instance.SetScoreText(0, 0);
            active = true;
            DialogueManager.instance.ActivateDialogue(entryDialogue);
            SetTriggers();
            spawnTimer.ResetTimer(false);
            spawnTimer.Initialize(spawnTime, false, false);
            spawnTimer.StartTimer();
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetTriggers()
    {
        foreach (MatchItemTrigger trigger in triggers)
        {
            if (trigger.gameObject.activeSelf == false)
            {
                trigger.gameObject.SetActive(true);
            }
            trigger.InitializeTrigger(this);
        }
    }

    //OBJECT POOL

    public void AddInactiveItem(MatchItem item)
    {
        if (activePool.Contains(item))
        {
            activePool.Remove(item);
        }
        inactivePool.Add(item);
        item.transform.SetParent(inactiveRoot);
        item.transform.position = spawnPoint.position;
        item.gameObject.SetActive(false);
    }

    public void AddActiveItem(MatchItem item)
    {
        if (inactivePool.Contains(item))
        {
            inactivePool.Remove(item);
        }
        activePool.Add(item);
        item.transform.SetParent(activeRoot);
        item.gameObject.SetActive(true);
        item.GetComponent<Rigidbody>().velocity = Vector3.zero;
        item.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * spawnForce, ForceMode.VelocityChange);
    }

    public MatchItem GetRandomInactiveItem()
    {
        MatchItem item = inactivePool[Random.Range(0, inactivePool.Count)];
        item.SetTarget(triggers[Random.Range(0, triggers.Count)]);
        return item;
    }

    public override void DeactivatePuzzle()
    {
        if(FirstPersonInteraction.instance.HeldObject() != null)
        {
            FirstPersonInteraction.instance.DropObject();
        }
        foreach(MatchItem item in activePool)
        {
            inactivePool.Add(item);
            item.transform.SetParent(inactiveRoot);
            item.transform.position = spawnPoint.position;
            item.gameObject.SetActive(false);
        }
        activePool.Clear();
        foreach (MatchItemTrigger trigger in triggers)
        {
            trigger.gameObject.SetActive(false);
        }
        active = false;
    }

    public bool QueryActivePoolRoom()
    {
        if(activePool.Count < activeLimit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
