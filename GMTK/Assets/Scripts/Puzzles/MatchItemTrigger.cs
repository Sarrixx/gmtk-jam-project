﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TriggerTag { none, red, green, blue, white }

public class MatchItemTrigger : MonoBehaviour
{
    public TriggerTag triggerTag = TriggerTag.red;
    public AudioClip correctClip;
    public AudioClip incorrectClip;

    private MatchItemPuzzle puzzleInstance;
    private bool active = false;
    private AudioSource aSrc;

    private void Awake()
    {
        TryGetComponent(out aSrc);
    }

    public void InitializeTrigger(MatchItemPuzzle puzzle)
    {
        puzzleInstance = puzzle;
        ActivateTrigger();
    }

    private void ActivateTrigger()
    {
        if(active == false)
        {
            active = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (active == true)
        {
            MatchItem item = other.GetComponent<MatchItem>();
            if (item != null)
            {
                if (item.Target().triggerTag == triggerTag || item.Target().triggerTag  == TriggerTag.none && item.Target() == this)
                {
                    //success
                    GameManager.GetCurrentPuzzle().AddScore(1);
                    HUD.instance.SetScoreText(GameManager.GetCurrentPuzzle().CurrentScore(), GameManager.GetCurrentPuzzle().targetScore);
                    aSrc.PlayOneShot(correctClip);
                    //Debug.Log($"Success - Item Trigger:{item.triggerType.ToString()} Target Trigger:{item.Target().triggerTag.ToString()} This Trigger:{triggerTag.ToString()}");
                }
                else
                {
                    //wrong trigger
                    GameManager.GetCurrentPuzzle().AddScore(-1);
                    HUD.instance.SetScoreText(GameManager.GetCurrentPuzzle().CurrentScore(), GameManager.GetCurrentPuzzle().targetScore);
                    aSrc.PlayOneShot(incorrectClip);
                    //Debug.Log($"Wrong trigger - Item Trigger:{item.triggerType.ToString()} Target Trigger:{item.Target().triggerTag.ToString()} This Trigger:{triggerTag.ToString()}");
                }
                puzzleInstance.AddInactiveItem(item);
            }
        }
    }
}
