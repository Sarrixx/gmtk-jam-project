﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPuzzle : Puzzle
{
    public List<Button> buttons;

    public Button CurrentButton { get; set; }

    public override bool ActivatePuzzle()
    {
        if(active == false)
        {
            currentScore = 0;
            HUD.instance.SetScoreText(0, targetScore);
            active = true;
            DialogueManager.instance.ActivateDialogue(entryDialogue);
            ActivateRandomButton();
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ActivateRandomButton()
    {
        if(CurrentButton != null && CurrentButton.Active() == true)
        {
            CurrentButton.Deactivate();
        }
        CurrentButton = GetRandomButton();
        CurrentButton.Activate();
    }

    private Button GetRandomButton()
    {
        int r = Random.Range(0, buttons.Count);
        return buttons[r];
    }

    public override void DeactivatePuzzle()
    {
        if (CurrentButton != null && CurrentButton.Active() == true)
        {
            CurrentButton.Deactivate();
        }
        active = false;
    }
}
