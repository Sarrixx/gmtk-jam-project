﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ButtonPuzzle), typeof(MatchItemPuzzle))]
public class BossFight : Puzzle
{
    public Vector2 actionTimeRange = new Vector2(5, 15);
    
    private ButtonPuzzle buttonPuzzle;
    private MatchItemPuzzle matchPuzzle;
    private Timer actionTimer = new Timer();

    public override void Awake()
    {
        base.Awake();
        buttonPuzzle = GetComponent<ButtonPuzzle>();
        matchPuzzle = GetComponent<MatchItemPuzzle>();
    }

    private void Start()
    {
        buttonPuzzle.OverrideTimer(puzzleTime);
        matchPuzzle.OverrideTimer(puzzleTime);
    }

    public override void Update()
    {
        base.Update();
        if (active == true)
        {
            if (actionTimer.QueryTimer() == true)
            {
                if (actionTimer.UpdateTimer() == true)
                {
                    if (buttonPuzzle.CurrentButton != null && buttonPuzzle.CurrentButton.Active() == true)
                    {
                        buttonPuzzle.CurrentButton.Deactivate();
                        GameManager.GetCurrentPuzzle().AddScore(-1);
                        HUD.instance.SetScoreText(GameManager.GetCurrentPuzzle().CurrentScore(), GameManager.GetCurrentPuzzle().targetScore);
                        buttonPuzzle.CurrentButton = null;
                    }


                    if (Random.Range(0, 2) == 0)
                    {
                        buttonPuzzle.ActivateRandomButton();
                    }
                    else
                    {
                        if (matchPuzzle.QueryActivePoolRoom() == true)
                        {
                            matchPuzzle.AddActiveItem(matchPuzzle.GetRandomInactiveItem());
                        }
                        else
                        {
                            buttonPuzzle.ActivateRandomButton();
                        }
                    }
                    actionTimer.ResetTimer(false);
                    SetTimerToRandom();
                }
            }
        }
    }

    public override bool ActivatePuzzle()
    {
        if (active == false)
        {
            currentScore = 0;
            HUD.instance.SetScoreText(0, GameManager.GetCurrentPuzzle().targetScore);
            DialogueManager.instance.ActivateDialogue(entryDialogue);
            active = true;
            matchPuzzle.SetTriggers();
            SetTimerToRandom();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void SetTimerToRandom()
    {
        float r = Random.Range(actionTimeRange.x, actionTimeRange.y);
        actionTimer.Initialize(r, false, false);
        actionTimer.StartTimer();
    }

    public override void DeactivatePuzzle()
    {
        matchPuzzle.DeactivatePuzzle();
        buttonPuzzle.DeactivatePuzzle();
        active = false;
    }
}
