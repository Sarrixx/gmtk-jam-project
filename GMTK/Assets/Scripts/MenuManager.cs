﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Camera[] cameras;
    public float camTime = 10f;
    public Animator canvasAnim;

    public static bool transition = false;

    private Timer camTimer = new Timer();
    private int camIndex = 0;

    private void Awake()
    {
        transition = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        camTimer.Initialize(camTime, false, false);
        SetCamera(camIndex);
        camTimer.StartTimer();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(camTimer.QueryTimer() == true && camTimer.UpdateTimer() == true)
        {
            StartCoroutine(FadeCamera());
            camTimer.ResetTimer(false);
        }
    }

    IEnumerator FadeCamera()
    {
        canvasAnim.SetTrigger("Fade");
        yield return new WaitForSeconds(1.5f);
        SetCamera(camIndex+1);
        canvasAnim.SetTrigger("Fade");
        camTimer.StartTimer();
    }

    void SetCamera(int index)
    {
        camIndex = index;
        if (index >= cameras.Length)
        {
            index = 0;
        }
        if (camIndex >= cameras.Length)
        {
            camIndex = 0;
        }
        for (int i = 0; i < cameras.Length; i++)
        {
            if(i != index)
            {
                cameras[i].gameObject.SetActive(false);
            }
            else
            {
                cameras[i].gameObject.SetActive(true);
            }
        }
    }

    public void StartGame()
    {
        if (transition == false)
        {
            transition = true;
            StartCoroutine(FadeToGame());
        }
    }

    public void Quit()
    {
        if (transition == false)
        {
            transition = true;
            StartCoroutine(FadeToQuit());
        }
    }

    IEnumerator FadeToGame()
    {
        if(transition == true)
        {
            canvasAnim.SetTrigger("Fade");
            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene(1);
        }
        else
        {
            yield return null;
        }
    }

    IEnumerator FadeToQuit()
    {
        if (transition == true)
        {
            canvasAnim.SetTrigger("Fade");
            yield return new WaitForSeconds(1.5f);
            Application.Quit();
        }
        else
        {
            yield return null;
        }
    }
}
