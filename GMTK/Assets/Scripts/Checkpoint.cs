﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Puzzle targetPuzzle;

    private bool transition = false;

    public void ActivateCheckpoint(float time)
    {
        if (transition == false)
        {
            StartCoroutine(ResetPlayer(time));
        }
    }

    public IEnumerator ResetPlayer(float waitTime = 0)
    {
        transition = true;
        FirstPersonController.canMove = false;
        FirstPersonController.instance.ResetPlayerMovement();
        HUD.instance.FadeHUD();
        if (waitTime == 0)
        {
            yield return new WaitForSeconds(1);
        }
        else
        {
            yield return new WaitForSeconds(waitTime);
        }
        FirstPersonController.instance.MoveToPosition(transform.position);
        HUD.instance.FadeHUD();
        yield return new WaitForSeconds(1);
        GameManager.SetPuzzle(targetPuzzle);
        FirstPersonController.canMove = true;
        transition = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && GameManager.checkPoint != this &&  GameManager.GetCurrentPuzzle() == null)
        {
            GameManager.checkPoint = this;
            GameManager.SetPuzzle(targetPuzzle);
            GetComponent<Collider>().enabled = false;
        }
    }
}
