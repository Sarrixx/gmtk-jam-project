﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && DialogueManager.instance.ActivateDialogue(dialogue) == true)
        {
            gameObject.SetActive(false);
        }
    }
}
