﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public List<DialogueMessage> messages;

    private DialogueMessage currentMessage;
    private Timer dialogueTimer = new Timer();

    public bool SetCurrentMessage(DialogueMessage message)
    {
        if (currentMessage == null)
        {
            currentMessage = message;
            if (currentMessage.clip != null)
            {
                dialogueTimer.Initialize(currentMessage.clip.length + DialogueManager.instance.messageBuffer, false, false);
                DialogueManager.instance.SetAudioClip(currentMessage.clip);
            }
            else
            {
                dialogueTimer.Initialize(DialogueManager.instance.messageBuffer, false, false);
            }
            if (currentMessage.message != null)
            {
                HUD.instance.SetSubtitles(currentMessage.message);
            }
            dialogueTimer.StartTimer();
            return true;
        }
        else
        {
            if (message == null)
            {
                currentMessage = null;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void Initialize()
    {
        SetCurrentMessage(messages[0]);
    }

    public void UpdateDialogue()
    {
        if(dialogueTimer.QueryTimer() == true)
        {
            if(dialogueTimer.UpdateTimer() == true)
            {
                dialogueTimer.ResetTimer(false);
                int nextIndex = messages.IndexOf(currentMessage) + 1;
                HUD.instance.SetSubtitles();
                SetCurrentMessage(null);
                DialogueManager.instance.SetAudioClip(null);
                if (nextIndex < messages.Count)
                {
                    SetCurrentMessage(messages[nextIndex]);
                }
                else
                {
                    Exit();
                }
            }
        }
    }

    private void Exit()
    {
        DialogueManager.instance.DeactivateDialogue();
    }

    public DialogueMessage CurrentMessage()
    {
        return currentMessage;
    }

}

[System.Serializable]
public class DialogueMessage
{
    [TextArea]
    public string message;
    public AudioClip clip;
}