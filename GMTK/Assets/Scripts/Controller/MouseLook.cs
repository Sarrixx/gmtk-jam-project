﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [Tooltip("The amount of influence mouse input has on camera movement.")]
    public float sensitivity;
    [Tooltip("The amount of 'drag' applied to the camera.")]
    public float smoothing;

    private Vector2 mLook;
    private Vector2 smoothVert;
    private GameObject charObj;
    private bool mouseLookEnabled = false;

    void Start()
    {
        charObj = transform.root.gameObject;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (mouseLookEnabled == true)
        {
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
            smoothVert.x = Mathf.Lerp(smoothVert.x, md.x, 1f / smoothing);
            smoothVert.y = Mathf.Lerp(smoothVert.y, md.y, 1f / smoothing);
            mLook += smoothVert;
            mLook.y = Mathf.Clamp(mLook.y, -70, 70);

            transform.localRotation = Quaternion.AngleAxis(-mLook.y, Vector3.right);
            charObj.transform.localRotation = Quaternion.AngleAxis(mLook.x, charObj.transform.up);
        }

        //if (Input.GetKeyDown(KeyCode.Comma))
        //{
        //    Cursor.visible = true;
        //    Cursor.lockState = CursorLockMode.None;
        //}
        //else if (Input.GetKeyDown(KeyCode.Period))
        //{
        //    Cursor.visible = false;
        //    Cursor.lockState = CursorLockMode.Locked;
        //}
    }

    public void ToggleMouseLook(bool toggle)
    {
        mouseLookEnabled = toggle;
    }

    public bool CanMouseLook()
    {
        return mouseLookEnabled;
    }
}
