﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    public AudioClip[] footsteps;
    public float stepTime = 0.5f;
    public bool varyPitch = false;

    private Timer stepTimer = new Timer();
    private FirstPersonController fpsController;
    private AudioSource aSrc;

    private void Awake()
    {
        fpsController = GetComponent<FirstPersonController>();
        aSrc = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        stepTimer.Initialize(stepTime, false, false, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (fpsController.enabled == true)
        {
            if (fpsController.IsMoving() == true && fpsController.IsGrounded() == true)
            {
                if (stepTimer.QueryTimer() == false)
                {
                    PlayRandomFootstep(varyPitch);
                    if (fpsController.Sprinting() == true)
                    {
                        stepTimer.ResetTimer(true, stepTime / 3 * 2 + (fpsController.CurrentSpeed()));
                    }
                    else
                    {
                        stepTimer.ResetTimer(true, stepTime + (fpsController.CurrentSpeed()));
                    }
                    stepTimer.ResetTimer(true);
                }
                else
                {
                    stepTimer.UpdateTimer();
                }
            }
            else
            {
                stepTimer.UpdateTimer();
            }
        }
        else if (aSrc.pitch != 1)
        {
            aSrc.pitch = 1;
        }
    }

    public void SprintSteps()
    {
        if(fpsController.Sprinting() == true)
        {
            stepTimer.Initialize(stepTime / 3 * 2, false, false, false);
        }
        else
        {
            stepTimer.Initialize(stepTime, false, false, false);
        }
    }

    void PlayRandomFootstep(bool varyPitch)
    {
        if (footsteps.Length > 0)
        {
            int random = Random.Range(0, footsteps.Length);
            if (varyPitch == true)
            {
                aSrc.pitch = Random.Range(0.95f, 1);
            }
            aSrc.PlayOneShot(footsteps[random]);
        }
    }
}
