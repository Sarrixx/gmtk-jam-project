﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    [Header("Movement Properties")]
    public float moveSpeed = 2.0f;
    [Tooltip("The amount that the move speed is multiplied by when sprinting.")]
    [Range(0.1f, 1)]
    public float sprintMultiplier = 0.25f;
    [Tooltip("The amount of distance above the player checked when crouching.")]
    [Range(1.1f, 2f)]
    public float headDetection = 1.25f;
    [Header("Gravity Properties")]
    [Tooltip("The amount of down force applied to the player.")]
    [Range(0.1f, 10)]
    public float gravity = 5.0f;
    [Tooltip("The amouont of force added to the players velocity when jumping.")]
    [Range(0.1f, 1)]
    public float jumpForce = 0.5f;
    [Tooltip("The maximum speed the player can achieve when falling.")]
    [Range(0.1f, 1)]
    public float fallSpeed;
    [Range(0, 1.5f)]
    public float jumpDelay = 0;
    [Header("Audio Properties")]
    public AudioClip[] jumpClips;

    public static bool canMove = true;
    public static FirstPersonController instance;
    public static PlayerCameraManager cameraManager = new PlayerCameraManager();

    private CharacterController controller;

    private bool isMoving = false;
    private bool isGrounded = false;
    private bool crouching = false;
    private bool invertZY = false;
    private bool jumpDelayEnabled = false;
    private float input_h = 0;
    private float input_v = 0;
    private float velocity = 0f;
    private float currentSpeed = 0f;
    private Vector3 move = Vector3.zero;
    private AudioSource aSrc;
    private Timer jumpDelayTimer = new Timer();

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        controller = GetComponent<CharacterController>();
        cameraManager.InitializeManager(gameObject);
        aSrc = GetComponent<AudioSource>();
    }

    private void Start()
    {
        currentSpeed = moveSpeed;
        jumpDelayTimer.Initialize(jumpDelay, false, false);
    }

    private void FixedUpdate()
    {
        move = Vector3.zero;
        isGrounded = controller.isGrounded;

        if (isGrounded == true)
        {
            velocity = -gravity * Time.fixedDeltaTime;

            if (jumpDelayTimer.QueryTimer() == true)
            {
                jumpDelayTimer.ResetTimer(false);
            }

            if (jumpDelayEnabled != true)
            {
                jumpDelayEnabled = true;
            }

            if (Input.GetButton("Crouch") == false && crouching == true && canMove == true)
            {
                Ray r = new Ray(transform.position, Vector3.up);
                if (Physics.SphereCast(r, 0.2f, out RaycastHit hit, headDetection))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        ToggleCrouch(false);
                    }
                }
                else
                {
                    ToggleCrouch(false);
                }
            }
        }
        else
        {
            if(jumpDelayTimer.QueryTimer() == false)
            {
                if (jumpDelayEnabled == true)
                {
                    jumpDelayTimer.StartTimer();
                }
            }

            if (Physics.SphereCast(transform.position, 0.2f, Vector3.up, out RaycastHit hit, headDetection))
            {
                velocity = -0.01f;
            }

            if (velocity > -fallSpeed)
            {
                velocity -= gravity * Time.fixedDeltaTime * jumpForce;
            }
            else
            {
                velocity = -fallSpeed;
            }
        }
    }

    void Update()
    {

        if (isGrounded == true)
        {
            if (canMove == true)
            {
                input_v = Input.GetAxisRaw("Vertical") * currentSpeed;
                input_h = Input.GetAxisRaw("Horizontal") * currentSpeed;
            }
            if (crouching == false)
            {
                if (Input.GetButtonDown("Jump") && canMove)
                {
                    Ray r = new Ray(transform.position, Vector3.up);
                    if (Physics.SphereCast(r, 0.2f, headDetection) == false)
                    {
                        velocity = jumpForce;
                        if (jumpClips.Length > 0)
                        {
                            aSrc.clip = jumpClips[Random.Range(0, jumpClips.Length)];
                            aSrc.pitch = 1;
                            aSrc.Play();
                        }
                        isGrounded = false;
                        jumpDelayTimer.ResetTimer(false);
                        jumpDelayEnabled = false;
                        cameraManager.SetAnimTrigger("jump");
                    }
                    else
                    {
                        Debug.DrawRay(r.origin, r.direction * headDetection, Color.red);
                    }
                }

                if (crouching == false)
                {
                    if (Input.GetButton("Sprint") == true && canMove)
                    {
                        ToggleSprint(true);
                    }
                    else
                    {
                        ToggleSprint(false);
                    }

                    if (Input.GetButton("Crouch") == true && canMove)
                    {
                        Ray r = new Ray(transform.position, Vector3.up);
                        if (Physics.SphereCast(r, 0.2f, out RaycastHit hit, headDetection) == false || hit.collider.tag == "Player")
                        {
                            ToggleCrouch(true);
                        }
                        else
                        {
                            ToggleCrouch(true);
                        }
                    }
                }
            }

            if (input_v != 0 || input_h != 0)
            {
                if (isMoving != true)
                {
                    isMoving = true;
                    cameraManager.SetAnimBool("isMoving", true);
                }
            }
            else
            {
                if (isMoving != false)
                {
                    isMoving = false;
                    cameraManager.SetAnimBool("isMoving", false);
                }
            }
        }
        else
        {
            if(jumpDelayTimer.QueryTimer() == true)
            {
                if (Input.GetButtonDown("Jump") && canMove)
                {
                    Ray r = new Ray(transform.position, Vector3.up);
                    if (Physics.SphereCast(r, 0.2f, headDetection) == false)
                    {
                        velocity = jumpForce;
                        if (jumpClips.Length > 0)
                        {
                            aSrc.clip = jumpClips[Random.Range(0, jumpClips.Length)];
                            aSrc.pitch = 1;
                            aSrc.Play();
                        }
                        isGrounded = false;
                        jumpDelayTimer.ResetTimer(false);
                        jumpDelayEnabled = false;
                    }
                    else
                    {
                        Debug.DrawRay(r.origin, r.direction * headDetection, Color.red);
                    }
                }

                Ray ra = new Ray(transform.position, Vector3.up);
                Debug.DrawRay(ra.origin, ra.direction * 1.1f, Color.red);
                if (Physics.SphereCast(ra, 0.2f, out RaycastHit hit, 1.1f) == true)
                {
                    if (hit.transform.tag != tag)
                    {
                        velocity = 0 + -(velocity/2);
                    }
                }

                if (jumpDelayTimer.UpdateTimer() == true)
                {
                    jumpDelayTimer.ResetTimer(false);
                    jumpDelayEnabled = false;
                }
            }

            if (invertZY == true && canMove == true)
            {
                input_v = Input.GetAxis("Vertical") * currentSpeed;
            }
            else if (canMove == true)
            {
                input_h = Input.GetAxis("Horizontal") * currentSpeed;
            }
        }

        ApplyMovement(input_v, input_h, velocity, move);
    }

    public void ApplyMovement(float v, float h, float velocity, Vector3 move)
    {
        if (invertZY == false)
        {
            move += transform.forward * v * currentSpeed * Time.deltaTime;
            move += transform.right * h * currentSpeed * Time.deltaTime;
            move.y += velocity;
        }
        else
        {
            move += transform.up * v * currentSpeed * Time.deltaTime;
        }
        controller.Move(move);
    }

    public void UpdatePosition(Vector3 m)
    {
        move = m / 100;
    }

    public void AddMovement(Vector3 m)
    {
        move += m;
    }

    private void ToggleSprint(bool toggle)
    {
        if (toggle == true)
        {
            currentSpeed = moveSpeed + (moveSpeed * sprintMultiplier);
            if(cameraManager.GetAnimBool("isSprinting") == false)
            {
                cameraManager.SetAnimBool("isSprinting", true);
            }
        }
        else
        {
            currentSpeed = moveSpeed;
            if (cameraManager.GetAnimBool("isSprinting") == true)
            {
                cameraManager.SetAnimBool("isSprinting", false);
            }
        }
        GetComponent<Footsteps>().SprintSteps();
    }

    public bool Sprinting()
    {
        if(currentSpeed > moveSpeed)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public float CurrentSpeed()
    {
        return currentSpeed;
    }

    public bool QueryFalling()
    {
        if (isGrounded == false)
        {
            if (velocity < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void SetVelocity(float value)
    {
        velocity = value;
    }

    public float GetVelocity()
    {
        return velocity;
    }

    private void ToggleCrouch(bool toggle)
    {
        if(toggle == true)
        {
            if(crouching == false)
            {
                crouching = true;
                controller.height /= 2;
                currentSpeed = moveSpeed - (moveSpeed * sprintMultiplier);
            }
        }
        else
        {
            if (crouching == true)
            {
                controller.height *= 2;
                currentSpeed = moveSpeed;
                crouching = false;
            }
        }
    }

    public void MoveToPosition(Vector3 position)
    {
        controller.enabled = false;
        transform.position = position;
        controller.enabled = true;
    }

    public bool IsMoving()
    {
        return isMoving;
    }

    public bool IsGrounded()
    {
        return isGrounded;
    }

    public bool ToggleLadder()
    {
        invertZY = !invertZY;
        return invertZY;
    }

    public void ResetPlayerMovement()
    {
        velocity = 0;
        input_h = 0;
        input_v = 0;
    }
}

[System.Serializable]
public class PlayerCameraManager
{
    public static PlayerCameraManager instance;
    public static Camera fpsCam;

    private Animator camAnim;
    private Camera deathCam;
    private MouseLook mouseLook;

    public void InitializeManager(GameObject player)
    {
        if(instance == null)
        {
            instance = this;
        }
        mouseLook = player.GetComponentInChildren<MouseLook>();
        foreach (Camera c in player.GetComponentsInChildren<Camera>())
        {
            if (c.tag == "FPSCamera" && fpsCam == null)
            {
                fpsCam = c;
                camAnim = c.GetComponent<Animator>();
            }
            else if (c.tag == "DeathCamera")
            {
                deathCam = c;
                deathCam.gameObject.SetActive(false);
            }

            if(fpsCam && deathCam != null)
            {
                break;
            }
        }
        mouseLook.ToggleMouseLook(true);
    }

    public void SetAnimBool(string name, bool toggle)
    {
        if (camAnim != null)
        {
            camAnim.SetBool(name, toggle);
        }
    }

    public bool GetAnimBool(string name)
    {
        if (camAnim != null)
        {
            return camAnim.GetBool(name);
        }
        else
        {
            return false;
        }
    }

    public void SetAnimTrigger(string name)
    {
        if (camAnim != null)
        {
            camAnim.SetTrigger(name);
        }
    }

    public void EnableDeathCam()
    {
        if(deathCam != null)
        {
            fpsCam.gameObject.SetActive(false);
            Rigidbody rb = deathCam.GetComponent<Rigidbody>();
            deathCam.transform.position = fpsCam.transform.position;
            deathCam.transform.rotation = fpsCam.transform.rotation;
            rb.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            rb.useGravity = true;
            rb.isKinematic = false;
            deathCam.gameObject.SetActive(true);
        }
    }
}