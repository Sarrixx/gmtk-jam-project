﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour
{
    public DialogueManager dialogueManager;

    public static GameManager instance;
    public static Checkpoint checkPoint;

    private static Puzzle currentPuzzle;
    private bool transition = false;
    private AudioSource aSrc;

    public AudioSource ManagerAudioSource()
    {
        return aSrc;
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        MenuManager.transition = false;
        aSrc = GetComponent<AudioSource>();
        dialogueManager.Awake();
    }

    private void Update()
    {
        if(DialogueManager.currentDialogue != null)
        {
            DialogueManager.currentDialogue.UpdateDialogue();
        }

        if (transition == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                StartCoroutine(QuitGame());
            }
        }
    }

    public static void SetPuzzle(Puzzle puzzle)
    {
        if(currentPuzzle == null)
        {
            currentPuzzle = puzzle;
            if(currentPuzzle.ActivatePuzzle() == true)
            {
                currentPuzzle.ActivatePuzzleTimer();
            }
        }
        else if (puzzle == null)
        {
            currentPuzzle = null;
        }
    }

    public static Puzzle GetCurrentPuzzle()
    {
        return currentPuzzle;
    }

    public static void Progress(Animator targetAnim, float time, bool endGame = false)
    {
        if (targetAnim != null)
        {
            instance.StartCoroutine(instance.TriggerAnimation(time, targetAnim));
        }

        if(endGame == true)
        {
            instance.StartCoroutine(instance.EndGame());
        }
    }

    public static void LoadCheckpoint(float time)
    {
        if(checkPoint != null)
        {
            checkPoint.ActivateCheckpoint(time);
        }
    }

    IEnumerator TriggerAnimation(float time, Animator targetAnim)
    {
        yield return new WaitForSeconds(time);
        targetAnim.SetTrigger("activate");
    }

    IEnumerator EndGame()
    {
        FirstPersonController.canMove = false;
        FirstPersonController.instance.ResetPlayerMovement();
        yield return new WaitForSeconds(DialogueManager.currentDialogue.CurrentMessage().clip.length / 2);
        HUD.instance.FadeHUD();
        yield return new WaitForSeconds(DialogueManager.currentDialogue.CurrentMessage().clip.length / 2 + 0.5f);
        FirstPersonController.canMove = true;
        FirstPersonController.instance = null;
        SceneManager.LoadScene(0);
    }

    IEnumerator QuitGame()
    {
        if (transition == false)
        {
            transition = true;
            FirstPersonController.canMove = false;
            FirstPersonController.instance.ResetPlayerMovement();
            HUD.instance.FadeHUD();
            yield return new WaitForSeconds(1.5f);
            Application.Quit();
        }
    }
}

[System.Serializable]
public class DialogueManager
{
    public float messageBuffer = 0.5f;

    public static DialogueManager instance;
    public static Dialogue currentDialogue;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        GameManager.instance.ManagerAudioSource().loop = false;
    }

    public bool ActivateDialogue(Dialogue dialogue)
    {
        if(currentDialogue == null)
        {
            currentDialogue = dialogue;
            currentDialogue.Initialize();
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool DeactivateDialogue()
    {
        if (currentDialogue != null)
        {
            currentDialogue = null;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SetAudioClip(AudioClip clip, bool oneShot = false)
    {
        if (clip != null)
        {
            if (oneShot == true)
            {
                GameManager.instance.ManagerAudioSource().PlayOneShot(clip);
                return true;
            }
            else
            {
                if (GameManager.instance.ManagerAudioSource().clip == null)
                {
                    GameManager.instance.ManagerAudioSource().clip = clip;
                    GameManager.instance.ManagerAudioSource().Play();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            if (GameManager.instance.ManagerAudioSource().clip != null)
            {
                if(GameManager.instance.ManagerAudioSource().isPlaying == true)
                {
                    return false;
                }
                else
                {
                    GameManager.instance.ManagerAudioSource().clip = null;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}