﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Image crosshair;
    public Image interact;
    public Text targetText;
    public Text scoreText;
    [Header("Subtitles")]
    public GameObject subtitlePanel;
    public Text subtitleText;

    public static HUD instance;

    private Animator anim;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            anim = GetComponent<Animator>();
        }
        SetTargetText("");
        SetScoreText(0, 0);
        SetSubtitles();
        interact.gameObject.SetActive(false);
    }

    public void SetCrosshairColour(Color colour)
    {
        if (crosshair.gameObject.activeSelf == true)
        {
            crosshair.color = colour;
        }
    }

    public void ToggleInteraction(bool toggle)
    {
        crosshair.gameObject.SetActive(!toggle);
        interact.gameObject.SetActive(toggle);
    }

    public void SetTargetText(string message)
    {
        targetText.text = $"Target: {message}";
    }

    public void SetScoreText(int value, int target)
    {
        scoreText.text = $"Score: {value.ToString()}";
        if (value > 0 && target > 0)
        {
            if (value >= target)
            {
                scoreText.color = Color.green;
            }
            else
            {
                scoreText.color = Color.red;
            }
        }
        else
        {
            scoreText.color = Color.red;
        }
    }

    public void SetSubtitles(string message = null)
    {
        if (message != null)
        {
            subtitleText.text = message.ToString();
            if (subtitlePanel.activeSelf == false)
            {
                subtitlePanel.SetActive(true);
            }
        }
        else
        {
            subtitleText.text = null;
            if (subtitlePanel.activeSelf == true)
            {
                subtitlePanel.SetActive(false);
            }
        }
    }

    public void FadeHUD()
    {
        anim.SetTrigger("Fade");
    }
}
