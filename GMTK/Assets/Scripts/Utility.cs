﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    /// <summary>
    /// Returns a child object of the instance of the defined layerName;
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="layerName"></param>
    /// <returns></returns>
    public static Transform FindChildOfLayer(Transform instance, string layerName)
    {
        Transform result = null;

        foreach(Transform child in instance.GetComponentsInChildren<Transform>())
        {
            if(LayerMask.LayerToName(child.gameObject.layer) == layerName)
            {
                result = child;
            }
        }
        return result;
    }
}

[System.Serializable]
public class Timer
{
    [SerializeField]
    private float timer = 0;
    [SerializeField]
    private float maxTime = 0f;
    [SerializeField]
    private bool invert = false, enabled = false, loop = false;

    /// <summary>
    /// Set time to maximum time for timer.
    /// Set invertTimer to true to make the timer count up instead of down.
    /// Set loopTimer to true to make sure the timer automatically resets when it reaches time.
    /// </summary>
    /// <param name="time"></param>
    /// <param name="invertTimer"></param>
    /// <param name="loopTimer"></param>
    public void Initialize(float time, bool invertTimer, bool loopTimer)
    {
        if (enabled == true)
        {
            ResetTimer(false);
        }
        maxTime = time;
        invert = invertTimer;
        loop = loopTimer;
    }

    /// <summary>
    /// Set time to maximum time for timer.
    /// Set invertTimer to true to make the timer count up instead of down.
    /// Set loopTimer to true to make sure the timer automatically resets when it reaches time.
    /// </summary>
    /// <param name="time"></param>
    /// <param name="invertTimer"></param>
    /// <param name="loopTimer"></param>
    /// <param name="disable"></param>
    public void Initialize(float time, bool invertTimer, bool loopTimer, bool disable)
    {
        if (enabled == true && disable == true)
        {
            ResetTimer(false);
        }
        maxTime = time;
        invert = invertTimer;
        loop = loopTimer;
    }

    /// <summary>
    /// Sets enabled to true;
    /// </summary>
    public void StartTimer()
    {
        enabled = true;
    }

    /// <summary>
    /// Runs every tick. Returns false if the timer is counting.
    /// </summary>
    /// <returns></returns>
    public bool UpdateTimer()
    {
        if (enabled == true)
        {
            if (invert == false)
            {
                if (timer < maxTime)
                {
                    timer += Time.deltaTime;
                    return false;
                }
                else
                {
                    if (loop == true)
                    {
                        ResetTimer(true);
                    }
                    else
                    {
                        ResetTimer(false);
                    }
                    return true;
                }
            }
            else
            {
                if (timer > maxTime)
                {
                    timer -= Time.deltaTime;
                    return false;
                }
                else
                {
                    if (loop == true)
                    {
                        ResetTimer(true);
                    }
                    else
                    {
                        ResetTimer(false);
                    }
                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Sets enabled to false.
    /// </summary>
    public void PauseTimer()
    {
        enabled = false;
    }

    /// <summary>
    /// Resets enabled and the value of the timer to 0.
    /// </summary>
    /// <param name="restart"></param>
    public void ResetTimer(bool restart)
    {
        if (enabled == true)
        {
            if (restart == false)
            {
                enabled = false;
            }
        }
        else
        {
            if(restart == true)
            {
                enabled = true;
            }
        }
        if (timer != 0)
        {
            timer = 0;
        }
    }


    /// <summary>
    /// Resets enabled and the value of the timer to 0, and sets the max time of the timer to newTime.
    /// </summary>
    /// <param name="restart"></param>
    /// <param name="newTime"></param>
    public void ResetTimer(bool restart, float newTime)
    {
        if (enabled == true)
        {
            if (restart == false)
            {
                enabled = false;
            }
        }
        else
        {
            if (restart == true)
            {
                enabled = true;
            }
        }
        if (timer != 0)
        {
            if (newTime > 0)
            {
                maxTime = newTime;
            }
            timer = 0;
        }
    }

    /// <summary>
    /// Returns true if the timer is actively counting.
    /// </summary>
    /// <returns></returns>
    public bool QueryTimer()
    {
        return enabled;
    }
}

[System.Serializable]
public class ObjectPool
{
    [Tooltip("Set this to the object to be spawned by the object pool.")]
    public GameObject objectPrefab;
    [Tooltip("Set this to the gameobject to spawn objects at the position of.")]
    public Transform spawnPoint;
    [Tooltip("The initial limit on the pool. This amount of objects will be loaded in when the pool is initialized.")]
    public int maxSpawns;

    private GameObject instance;
    [SerializeField]
    private List<GameObject> activePool = new List<GameObject>();
    [SerializeField]
    private List<GameObject> objectPool = new List<GameObject>();
    private int activeObjIndex = 0;

    /// <summary>
    /// Sets the instance of the pool. Initializes spawning of pool.
    /// </summary>
    /// <param name="i"></param>
    public void InitializePool(GameObject i, bool spawnPool = true)
    {
        instance = i;
        if (spawnPoint == null)
        {
            spawnPoint = instance.transform;
        }
        SpawnPool(spawnPool);
    }

    /// <summary>
    /// Sets the instance of the pool. Initializes spawning of pool. Sets pool amount.
    /// </summary>
    /// <param name="i"></param>
    public void InitializePool(GameObject i, int spawnAmount, bool spawnPool = true)
    {
        instance = i;
        if(spawnPoint == null)
        {
            spawnPoint = instance.transform;
        }
        if(spawnAmount > 0)
        {
            maxSpawns = spawnAmount;
        }
        SpawnPool(spawnPool);
    }

    /// <summary>
    /// Spawns in maxSpawns amount of objects. If deactivate is set to true, the objects will be disabled after being spawned.
    /// </summary>
    /// <param name="deactivate"></param>
    public void SpawnPool(bool deactivate)
    {
        for (int i = objectPool.Count; i < maxSpawns; i++)
        {
            GameObject spawn = Object.Instantiate(objectPrefab, spawnPoint.position, objectPrefab.transform.rotation, instance.transform);
            spawn.name = objectPrefab.name;
            activePool.Add(spawn);
            if (deactivate == true)
            {
                DeactivateObject(spawn);
            }
        }
    }

    /// <summary>
    /// Increases the amount of maximum spawns by amount and spawns extra objects.
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    public GameObject AddObjects(int amount, bool activate = true)
    {
        if(IsPoolFull() == true)
        {
            GameObject lastSpawn = null;
            maxSpawns += amount;
            for (int i = activePool.Count; i < maxSpawns; i++)
            {
                lastSpawn = Object.Instantiate(objectPrefab, spawnPoint.position, objectPrefab.transform.rotation, instance.transform);
                lastSpawn.name = objectPrefab.name;
                activePool.Add(lastSpawn);
                if(activate == false)
                {
                    lastSpawn.SetActive(false);
                }
            }
            return lastSpawn;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Adds obj to the pool and spawns it into the scene.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public GameObject AddObject(GameObject obj)
    {
        GameObject spawn = null;
        maxSpawns++;
        spawn = Object.Instantiate(obj, spawnPoint.position, obj.transform.rotation, instance.transform);
        spawn.name = obj.name;
        activePool.Add(spawn);
        return spawn;
    }

    /// <summary>
    /// Activates a disabled object from the pool, or spawns another one in if no active objects are available.
    /// </summary>
    /// <returns></returns>
    public GameObject ActivateObject(bool useActiveObject = false)
    {
        if (objectPool.Count > 0 && IsPoolFull() == false)
        {
            GameObject obj = null;
            foreach (GameObject go in objectPool)
            {
                if (go.activeSelf == false)
                {
                    obj = go;
                    activePool.Add(obj);
                    objectPool.Remove(obj);
                    obj.transform.position = spawnPoint.position;
                    obj.SetActive(true);
                    break;
                }
            }
            return obj;
        }
        else
        {
            if (objectPrefab != null)
            {
                if (useActiveObject == false)
                {
                    return AddObjects(1);
                }
                else
                {
                    return UseActiveObject();
                }
            }
            else
            {
                return null;
            }
        }
    }

    GameObject UseActiveObject()
    {
        GameObject obj = activePool[activeObjIndex];
        activeObjIndex++;
        if(activeObjIndex >= activePool.Count)
        {
            activeObjIndex = 0;
        }
        return obj;
    }

    /// <summary>
    /// Finds obj in the active pool, disables it, and moves it to the inactive pool.
    /// </summary>
    /// <param name="obj"></param>
    public void DeactivateObject(GameObject obj)
    {
        if (activePool.Contains(obj))
        {
            objectPool.Add(obj);
            activePool.Remove(obj);
            obj.SetActive(false);
        }
    }

    /// <summary>
    /// Returns the list of inactive pool objects.
    /// </summary>
    /// <returns></returns>
    public List<GameObject> GetPool()
    {
        return objectPool;
    }

    /// <summary>
    /// Returns the list of active pool objects.
    /// </summary>
    /// <returns></returns>
    public List<GameObject> GetActivePool()
    {
        return activePool;
    }

    /// <summary>
    /// Returns true of the active pool is full.
    /// </summary>
    /// <returns></returns>
    public bool IsPoolFull()
    {
        if (activePool.Count > 0)
        {
            if (activePool.Count < maxSpawns)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    public void ClearPools()
    {
        activePool.Clear();
        objectPool.Clear();
    }
}