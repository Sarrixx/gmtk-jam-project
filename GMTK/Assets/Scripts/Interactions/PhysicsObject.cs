﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : Interaction
{
    public override bool Activate()
    {
        if (active == false)
        {
            FirstPersonInteraction.instance.PickupObject(gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Deactivate()
    {
        if (active == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
