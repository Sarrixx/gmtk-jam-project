﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Button : Interaction
{
    public AudioClip activeClip;
    public float pingInterval = 1f;

    private Animator anim;
    private AudioSource aSrc;

    private float timer = -1;

    private void Awake()
    {
        aSrc = GetComponent<AudioSource>();
        anim = GetComponentInChildren<Animator>();
        canActivate = false;
    }

    private void Update()
    {
        if(active == true && activeClip != null)
        {
            if(timer >= 0)
            {
                timer += Time.deltaTime;
                if(timer > pingInterval)
                {
                    timer = 0;
                    aSrc.PlayOneShot(activeClip);
                }
            }
        }
    }

    public override bool Activate()
    {
        if(active == false)
        {
            active = true;
            anim.SetBool("active", true);
            if(interactionClip != null)
            {
                PlayInteractionClip();
            }
            if(activeClip != null)
            {
                aSrc.PlayOneShot(activeClip);
                timer = 0;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Deactivate()
    {
        if(active == true)
        {
            active = false;
            anim.SetBool("active", false);
            if(activeClip != null)
            {
                timer = -1;
            }
            if (GameManager.GetCurrentPuzzle() != null)
            {
                if (GameManager.GetCurrentPuzzle().GetType() == typeof(ButtonPuzzle))
                {
                    GameManager.GetCurrentPuzzle().GetComponent<ButtonPuzzle>().ActivateRandomButton();
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlayInteractionClip()
    {
        aSrc.PlayOneShot(interactionClip);
    }
}
