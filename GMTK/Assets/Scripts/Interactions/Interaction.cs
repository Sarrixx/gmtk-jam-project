﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interaction : MonoBehaviour
{
    public bool canActivate = true;
    public AudioClip interactionClip;

    protected bool active = false;

    public abstract bool Activate();

    public abstract bool Deactivate();

    public bool Active()
    {
        return active;
    }
}
