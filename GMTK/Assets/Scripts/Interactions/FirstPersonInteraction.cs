﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonInteraction : MonoBehaviour
{
    public float distance = 1.5f;
    public float throwForce = 2.5f;
    public float collisionBuffer = 0.5f;
    public Vector3 itemOffset;
    public LayerMask interactionLayer;
    public Sprite interactionSprite;

    public static FirstPersonInteraction instance;

    private Camera cam;
    private Transform heldObject;
    private FirstPersonController controller;

    public Transform HeldObject()
    {
        return heldObject;
    }

    private void Awake()
    {
        controller = GetComponent<FirstPersonController>();
        if(instance == null)
        {
            instance = this;
        }
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (heldObject != null)
            {
                ThrowObject();
            }
            else
            {
                CheckInteraction();
            }
        }

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out RaycastHit detection, distance, interactionLayer, QueryTriggerInteraction.Ignore) == true && heldObject == null)
        {
            HUD.instance.SetCrosshairColour(Color.green);
            HUD.instance.ToggleInteraction(true);
        }
        else
        {
            HUD.instance.ToggleInteraction(false);
            HUD.instance.SetCrosshairColour(Color.red);
        }
    }

    void CheckInteraction()
    {
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out RaycastHit detection, distance, interactionLayer, QueryTriggerInteraction.Ignore) == true)
        {
            if (detection.transform.GetComponent<Interaction>())
            {
                Debug.DrawRay(cam.transform.position, cam.transform.forward * distance, Color.green, 0.5f);
                Interaction data = detection.transform.GetComponent<Interaction>();
                if (data.Active() == true)
                {
                    data.Deactivate();
                    if(data.GetType() == typeof(Button))
                    {
                        if(GameManager.GetCurrentPuzzle().GetType() == typeof(BossFight) || GameManager.GetCurrentPuzzle().GetType() == typeof(ButtonPuzzle))
                        {
                            GameManager.GetCurrentPuzzle().AddScore(1);
                            HUD.instance.SetScoreText(GameManager.GetCurrentPuzzle().CurrentScore(), GameManager.GetCurrentPuzzle().targetScore);
                            data.GetComponent<Button>().PlayInteractionClip();
                        }
                    }
                }
                else if (data.canActivate == true)
                {
                    if(data.Activate() == false)
                    {
                        data.Deactivate();
                    }
                }
            }
        }
    }

    public void PickupObject(GameObject obj)
    {
        if (heldObject == null)
        {
            heldObject = obj.transform;
            Rigidbody rb = heldObject.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeAll;
            rb.useGravity = false;
            heldObject.GetComponent<Collider>().enabled = false;
            heldObject.SetParent(cam.transform);
            heldObject.localPosition = itemOffset;
            heldObject.localEulerAngles = Vector3.zero;
            heldObject.gameObject.layer = LayerMask.NameToLayer("Held Object");
            MatchItem item = heldObject.GetComponent<MatchItem>();
            if (item != null)
            {
                if (item.triggerType != TriggerTag.none)
                {
                    HUD.instance.SetTargetText(item.triggerType.ToString().ToUpper());
                }
                else
                {
                    HUD.instance.SetTargetText(item.Target().name.ToString().ToUpper());
                }
            }
        }
    }

    public Transform DropObject()
    {
        Rigidbody rb = heldObject.GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.None;
        //rb.constraints = RigidbodyConstraints.FreezeRotation;
        rb.useGravity = true;
        heldObject.GetComponent<Collider>().enabled = true;
        heldObject.parent = null;
        heldObject.eulerAngles = new Vector3(0, heldObject.eulerAngles.y, 0);
        Transform t = heldObject;
        heldObject.gameObject.layer = LayerMask.NameToLayer("Interaction");
        heldObject = null;
        HUD.instance.SetTargetText("");
        return t;
    }

    public void ThrowObject()
    {
        Transform obj = DropObject();
        if (obj != null)
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            rb.AddForce(cam.transform.forward * throwForce, ForceMode.Impulse);
        }
    }
}
