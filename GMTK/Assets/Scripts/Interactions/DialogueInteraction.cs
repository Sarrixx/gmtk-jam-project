﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueInteraction : Interaction
{
    public Dialogue dialogue;
    [Header("Animation Trigger Settings")]
    public Animator target;
    public bool isButton = false;
    public AudioClip buttonSound;

    private Animator btnAnim;
    private AudioSource aSrc;

    private void Awake()
    {
        if (isButton == true)
        {
            btnAnim = GetComponent<Animator>();
            btnAnim.SetBool("active", true);
        }
        aSrc = GetComponent<AudioSource>();
    }

    public override bool Activate()
    {
        if (active == false)
        {
            active = true;
            StartCoroutine(TriggerDialogue());
            if (isButton)
            {
                if (buttonSound != null)
                {
                    aSrc.PlayOneShot(buttonSound);
                }
                btnAnim.SetBool("active", false);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Deactivate()
    {
        return false;
    }

    IEnumerator TriggerDialogue()
    {
        DialogueManager.instance.ActivateDialogue(dialogue);
        List<AudioClip> clips = new List<AudioClip>();
        float time = 0;
        foreach (DialogueMessage message in dialogue.messages)
        {
            if(message.clip != null)
            {
                clips.Add(message.clip);
            }
            else
            {
                time += DialogueManager.instance.messageBuffer;
            }
        }
        foreach(AudioClip clip in clips)
        {
            time += clip.length;
        }
        yield return new WaitForSeconds(time);
        if(target != null)
        {
            target.SetTrigger("activate");
            if (interactionClip != null)
            {
                aSrc.PlayOneShot(interactionClip);
            }
        }
    }
}
